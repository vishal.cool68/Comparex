 

import { Http} from '@angular/http'; 
import { Headers, RequestOptions } from '@angular/http';

import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable'; 
import 'rxjs/add/operator/toPromise';
 import { AlertController } from 'ionic-angular'; 
 
import {  ActionSheetController,
  ToastController, Platform,
  LoadingController, Loading, Events,ModalController } from 'ionic-angular';
 
  // var headers = new Headers();
  // headers.append('Access-Control-Allow-Origin' , '*');
  // headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
  // headers.append('Accept','application/json');
  // headers.append('content-type','application/json'); 

  
@Injectable()

export class WebserviceProvider { 
  lastImage: string = null;
  docParams: any;
  data:any; 
 
 imageBaseURL:any='';   
 
 api:any='http://182.71.1.166:11026';       
 
  apiUrlv1:any = this.api+'/hrodportalservice.svc/';   
 
   loader: any;

   accessToken: any;
   
  constructor(public http: Http, 
              public loading: LoadingController,
              public actionSheetCtrl: ActionSheetController,
              public toastCtrl: ToastController,
              public platform: Platform,
              public events: Events,
              private alertCtrl: AlertController, 
              public modalCtrl:ModalController
              ) { 

    console.log('Hello WebserviceProvider Provider');
  }



  // login_user(bodystring){
 
  //   this.datafetch(); 
  
 
  //   return new Promise(resolve => { 
  //     this.http.get(this.apiUrlv1+'remoteLogin',bodystring) 
  //     .map(res => res.json()) 
  //     .subscribe(data => {
  //       this.data = data;
  
  //       this.loader.dismissAll();
  
  //       console.log("logindatap"+ JSON.stringify(data.data));
   
  //       localStorage.setItem("logindata", JSON.stringify(data.data));
  
   
  //       let loggedUsername=data.data.name.replace("&amp;", "&");
  
  //       localStorage.setItem('loggedUsername', loggedUsername); 
  //       this.events.publish("Update:loggedUsername");   
  //       resolve(this.data); 

  //     }, (err) => {
  //       console.log(err);
  //       this.loader.dismissAll();
  
  //       if(400){
          
  //           this.presentToast(JSON.stringify(err.json().message));
  //            console.log('error msg vishal'+JSON.stringify(err.json().message));   
  //       }
  //       else{
  //          this.showAlert("Ooops!! Some problem is there.");
  
  //       }
  
  //     });
  //   });
  // } 


 

  public login_user_get(username,password) {
 
    this.datafetch();  

      let headers = new Headers();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept','application/json');
      headers.append('content-type','application/json');
      
    let options = new RequestOptions({ headers: headers });   


    return new Promise(resolve => { 
     this.http.get(this.apiUrlv1+'UserAuthenticationTokenJSON/'+username+'/'+password,options)  
      .map(res => res.json()) 
        .subscribe(data => {  
        this.data = data;   
         resolve(this.data); 

         this.loader.dismissAll();

         }, (err) => {
               console.log(err);
               this.loader.dismissAll();
       if(400){
              resolve(err.json());
              this.showAlert("Ooops!! Some problem is there.");
               this.loader.dismissAll();
          }
      else{
        this.showAlert("Ooops!! Some problem is there.");
           this.loader.dismissAll();
      }
    });
  });
}
 
 

public SAVEStartFromData(StartFrombodystring){ 


  this.datafetch();  

  let headers = new Headers();
  headers.append('Access-Control-Allow-Origin' , '*');
  headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
  headers.append('Accept','application/json');
  headers.append('content-type','application/json');
  let options = new RequestOptions({ headers: headers });  

  return new Promise(resolve => { 

   this.http.post(this.apiUrlv1+'InsertDataIntoODTableJSON',StartFrombodystring,options) 
   .map(res => res.json())
   .subscribe(data => { 

    this.loader.dismissAll();

     this.data = data;

      console.log(JSON.stringify(data));

     resolve(this.data);
     }, (err) => {
     console.log(err);
     this.loader.dismissAll();
     if(400){
     resolve(err.json());
     this.showAlert("Ooops!! Some problem is there.");
     this.loader.dismissAll();

     }
     else{
       this.showAlert("Ooops!! Some problem is there."); 
       this.loader.dismissAll();
     }

   });
 });
}


public SAVETimeINData(bodystring){ 

  let headers = new Headers();
  headers.append('Access-Control-Allow-Origin' , '*');
  headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
  headers.append('Accept','application/json');
  headers.append('content-type','application/json');
  let options = new RequestOptions({ headers: headers });  

  return new Promise(resolve => { 

   this.http.put(this.apiUrlv1+'UpdateFirstIntoODTableJSON',bodystring,options) 
   .map(res => res.json())
   .subscribe(data => { 
     this.data = data;

      console.log(JSON.stringify(data));

     resolve(this.data);
     }, (err) => {
     console.log(err);
     if(400){
     resolve(err.json());
     this.showAlert("Ooops!! Some problem is there.");

     }
     else{
       this.showAlert("Ooops!! Some problem is there.");
     }

   });
 });
}



  showAlert(msg){
    let alert = this.alertCtrl.create({
      title: 'Comparex HR Portal',
      subTitle: msg,
      buttons: ['Ok']
    });
    alert.present();
  }



  datafetch()
  {
      this.loader = this.loading.create({
        content: 'Please wait....' 
         }); 
      this.loader.present(); 
   }


   Timeoutdatafetch()
  {
      this.loader = this.loading.create({
        content: 'Updating all Captured Information...' 
         }); 
      this.loader.present(); 
   }


  public presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }


  public sendEmail(username,finalAddress,CurrentID,LastInsertedDate) {
 
  //  this.datafetch();  

      let headers = new Headers();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept','application/json');
      headers.append('content-type','application/json');
      
    let options = new RequestOptions({ headers: headers });   


    return new Promise(resolve => { 
      
    this.http.get('http://mytechnocrates.com/sendmail.php?username='+username+'&address='+finalAddress+'&CurrentID='+CurrentID+'&LastInsertedDate='+LastInsertedDate,options)  
      .map(res => res) 
        .subscribe(data => {  
        this.data = data;   
         resolve(this.data); 

        // this.loader.dismissAll();

         }, (err) => {
               console.log(err);
               this.loader.dismissAll();
       if(400){
              resolve(err.json());
              this.showAlert("Ooops!! Some problem is there.");
              // this.loader.dismissAll();
          }
      else{
        this.showAlert("Ooops!! Some problem is there.");
         //  this.loader.dismissAll();
      }
    });
  });
}


public APICALLOD(username) {
 
  this.datafetch();  

    let headers = new Headers();
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    
  let options = new RequestOptions({ headers: headers });   

  //182.71.1.166:11009/Service1.svc/RestAPIService/CheckLastTimeOutJSON/ashwini.kumar1


  return new Promise(resolve => { 

  this.http.get(this.apiUrlv1+'CheckLastTimeOutJSON/'+username,options)  
    .map(res => res.json()) 
       .subscribe(data => {  
      this.data = data;   
       resolve(this.data);  

       this.loader.dismissAll();

       }, (err) => {
             console.log(err);
             this.loader.dismissAll();
     if(400){
            resolve(err.json());
            this.showAlert("Ooops!! Some problem is there.");
             this.loader.dismissAll();
        }
    else{
      this.showAlert("Ooops!! Some problem is there.");
         this.loader.dismissAll();
    }
  });
});
}

 
public UpdateTimePotData(timeoutbodystring){ 

  let headers = new Headers();
  headers.append('Access-Control-Allow-Origin' , '*');
  headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
  headers.append('Accept','application/json');
  headers.append('content-type','application/json');
  let options = new RequestOptions({ headers: headers });  

  return new Promise(resolve => { 

   this.http.put(this.apiUrlv1+'UpdateLastIntoODTableJSON',timeoutbodystring,options) 
   .map(res => res.json())
   .subscribe(data => { 
     this.data = data;

      console.log(JSON.stringify(data)); 

     resolve(this.data);
     }, (err) => {
     console.log(err);
     if(400){
     resolve(err.json());
     this.showAlert("Ooops!! Some problem is there.");

     }
     else{
       this.showAlert("Ooops!! Some problem is there.");
     }

   });
 });
}


public ALLUsersList(username) {
 
  this.datafetch();  

    let headers = new Headers();
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    
  let options = new RequestOptions({ headers: headers });    
 
  return new Promise(resolve => { 

  this.http.get(this.apiUrlv1+'GetUserDetailsJSON/'+username,options)  
    .map(res => res.json()) 
       .subscribe(data => {  
      this.data = data;   
       resolve(this.data);  
       this.loader.dismissAll(); 
       }, (err) => {
             console.log(err);
             this.loader.dismissAll();
     if(400){
            resolve(err.json());
            this.showAlert("Ooops!! Some problem is there.");
             this.loader.dismissAll();
        }
    else{
      this.showAlert("Ooops!! Some problem is there.");
         this.loader.dismissAll();
    }
  });
});
}


public ListManagerODApproval(managerName) {
 
  this.datafetch();  

    let headers = new Headers();
    headers.append('Access-Control-Allow-Origin' , '*');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    
  let options = new RequestOptions({ headers: headers });   
 
  return new Promise(resolve => { 

  this.http.get(this.apiUrlv1+'GetManagerDetailsJSON/'+managerName,options)  
    .map(res => res.json()) 
       .subscribe(data => {  
      this.data = data;   
       resolve(this.data);  

       this.loader.dismissAll(); 

       }, (err) => {
             console.log(err);
             this.loader.dismissAll();
     if(400){
            resolve(err.json());
            this.showAlert("Ooops!! Some problem is there.");
             this.loader.dismissAll();
        }
    else{
      this.showAlert("Ooops!! Some problem is there.");
         this.loader.dismissAll();
    }
  });
});
}



}
