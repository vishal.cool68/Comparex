import { Component,ViewChild,ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
 import { Content } from 'ionic-angular'; 
  
import {Nav,ActionSheetController, LoadingController,Platform } from 'ionic-angular';
  
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions,LatLng, CameraPosition, MarkerOptions, Marker,MyLocation,GoogleMapsAnimation} from '@ionic-native/google-maps'; 

import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation'; 
import { JsonPipe } from '@angular/common';
 
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
     
import { WebserviceProvider } from '../../providers/webservice/webservice';

import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network'; 
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { InAppBrowser } from '@ionic-native/in-app-browser';
 import { Http} from '@angular/http'; 
import { Headers, RequestOptions } from '@angular/http';
import { AlertController } from 'ionic-angular';

 
@Component({
  selector: 'page-managerodapproval',
  templateUrl: 'managerodapproval.html',
})
export class ManagerodapprovalPage {
  @ViewChild(Content) content: Content;

  scrollToTop() {
    this.content.scrollToTop(); 
  }

  managerresult:any;
UserName:any;
UserLoginID:any;
ManagerName:any;
email:string; 
isAndroid: boolean = false; 
 
    constructor(public navCtrl: NavController,
      public platform: Platform,private device: Device,private network: Network,
      private googleMaps: GoogleMaps,
      private locationAccuracy: LocationAccuracy,
      private iab: InAppBrowser,private geolocation: Geolocation 
      ,private nativeGeocoder: NativeGeocoder,
      private webservice: WebserviceProvider,
       public http: Http,
      private alertCtrl: AlertController ) {

        let loggedData = JSON.parse(localStorage.getItem('loggedData'));  
        this.email=loggedData.EmailID; 
        this.UserName=loggedData.UserName;
        this.UserLoginID=loggedData.UserLoginID;
        this.ManagerName=loggedData.ManagerName;
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ManagerodapprovalPage');
    this.ManagerODApprovalList(this.UserLoginID);
  }


ManagerODApprovalList(Username){
  this.webservice.datafetch();
   this.webservice.ListManagerODApproval(Username) 
  .then(response => {  
    this.webservice.loader.dismissAll(); 
    this.managerresult=response;  
}, (err) => { 
    console.log("Error"+err);
}); 


}


}
