import { Component,ViewChild, ElementRef  } from '@angular/core'; 
  import { Content } from 'ionic-angular';

import {Nav, NavController, ActionSheetController, LoadingController,Platform } from 'ionic-angular';
  
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions,LatLng, CameraPosition, MarkerOptions, Marker,MyLocation,GoogleMapsAnimation} from '@ionic-native/google-maps'; 

import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation'; 
import { JsonPipe } from '@angular/common';
 
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
     
import { WebserviceProvider } from '../../providers/webservice/webservice';

import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network'; 
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmailComposer } from '@ionic-native/email-composer';
import { Http} from '@angular/http'; 
import { Headers, RequestOptions } from '@angular/http';
import { AlertController } from 'ionic-angular';
 
declare var google; 

 @Component({
  selector: 'page-od',
  templateUrl: 'od.html',
})
export class OdPage {

  
  @ViewChild(Content) content: Content;

  scrollToTop() {
    this.content.scrollToTop(); 
  }

  
  srcImage: string;
  OCRAD: any;
  @ViewChild('map') mapElement: ElementRef; 
  map: any;
  // map: GoogleMap; 
  options : GeolocationOptions;
  currentPos : Geoposition; 
  value:any;
  lat:any;
  long:any;

  markers: any;  
 finaladdress:any; 
 
establishments:any;

countryCode:any;
countryName:any;
postalCode:any;
administrativeArea:any;
subAdministrativeArea:any;
locality:any;
subLocality:any; 
finalAddress:any;
email:string; 
profileModel: string = "profile";
isAndroid: boolean = false; 
timeINtitle:any;

timeIStartFromResponse=false; 
timeININsertResponse=false;
timeOUTupdateResponse=false;
saveFinaladdress: Array<{}>; 

CurrentID:any; 
StartFromCurrentID:any; 
TimeInCurrentID:any; 

LastInsertedDate:any; 
ResultData:any;

LastlocationGPS:any;
StartFromlocationGPS:any;
lastlat:any;
lastlong:any;
UserName:any;
UserLoginID:any;
ManagerName:any;
userresult:any;
//showUserDetails:boolean; 
showUserDetails=false; 
selectedSection:any;

StartFromLocation:any; 
TimeOutFinaladdress:any; 

StartFromLocationfinal :any; 



constructor(public navCtrl: NavController,
  public platform: Platform,private device: Device,private network: Network,
  private googleMaps: GoogleMaps,
  private locationAccuracy: LocationAccuracy,
  private iab: InAppBrowser,private geolocation: Geolocation 
  ,private nativeGeocoder: NativeGeocoder,
  private webservice: WebserviceProvider,
  private emailComposer: EmailComposer,
  public http: Http,
  private alertCtrl: AlertController ) { 
    
 

    this.isAndroid = platform.is('android');

    let loggedData = JSON.parse(localStorage.getItem('loggedData'));   

    this.email=loggedData.EmailID; 
    this.UserName=loggedData.UserName;
    this.UserLoginID=loggedData.UserLoginID;
    this.ManagerName=loggedData.ManagerName;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OdPage');
    this.APICall();
  }

  
  FindLocation(){
     
    this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 10000, enableHighAccuracy: true }).then((resp) => {
    
       let location = new LatLng(resp.coords.latitude, resp.coords.longitude);
  
       console.log('location'+JSON.stringify(location));
       this.presentPrompt(location.lat,location.lng); 

       this.loadMap(location.lat,location.lng);     
}); 

 
  }

 
  loadMap(lat,long){
     
    let latLng = new google.maps.LatLng(lat, long);  
    let mapOption = {
     center: latLng,
     zoom: 14,
    // mapTypeId:'roadmap',
     mapTypeId: google.maps.MapTypeId.ROADMAP,
     disableDefaultUI: true 
   }

  
   let element = document.getElementById('map');
   this.map = new google.maps.Map(element, mapOption);

    //this.map = new google.maps.Map(this.mapElement.nativeElement, mapOption);

    let marker = new google.maps.Marker({
     position: latLng,
     title: 'Current Location',
   //  icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
   })
   
   let content = ` 
     <div id="myId" class="item item-thumbnail-left item-text-wrap">
       <ion-item>
         <ion-row>
           <h6> `+marker.title+`</h6>
           <h6> `+ marker.position +`</h6>
         </ion-row>
       </ion-item>
     </div>
   `
   this.addInfoWindow(marker, content);
   marker.setMap(this.map); 

   this.loadPoints(lat,long);  

}



loadPoints(lat,long){ 
let options: NativeGeocoderOptions = {
  useLocale: true,
  maxResults: 5
  }; 


this.nativeGeocoder.reverseGeocode(lat,long, options) 
.then((result: NativeGeocoderReverseResult[]) =>      
{


this.countryCode=result[0].countryCode; 
this.countryName=result[0].countryName;
this.postalCode=result[0].postalCode;
this.administrativeArea=result[0].administrativeArea;
this.subAdministrativeArea=result[0].subAdministrativeArea;
this.locality=result[0].locality;
this.subLocality=result[0].subLocality; 
 
// this.finalAddress= 'Admistrative Area : '+this.administrativeArea+', Sub Administrative Area : '+this.subAdministrativeArea+' , Locality : '+ this.locality+' , Sub locality : '+this.subLocality+' ,  Country Code : ' + this.countryCode+ ' ,  Country name:  ' + this.countryName+', Postal Code : '+this.postalCode;

this.finalAddress='State: '+this.administrativeArea+', ' +this.subAdministrativeArea
                  +' , '+ this.locality + ' , '+this.subLocality+ ' ,  Country:  ' 
                  + this.countryName+', Postal Code : '+this.postalCode;



this.TimeOutFinaladdress= this.finalAddress;   
  
 localStorage.setItem('finalAddress', this.finalAddress); 
localStorage.setItem('TimeOutFinaladdress', this.TimeOutFinaladdress);    



this.establishments = [{
  name: this.finalAddress, 
  lat: lat,    
  lng:long 
  }]  


  this.markers = [];
  for (const key of Object.keys(this.establishments)){
    let latLng = new google.maps.LatLng(this.establishments[key].lat, this.establishments[key].lng);
    let marker = new google.maps.Marker({
      position: latLng,
      title: this.establishments[key].name
    })
    let content = `
   <div id="myId" class="item item-thumbnail-left item-text-wrap">
     <ion-item>
       <ion-row>
         <h6> `+ this.establishments[key].name + `</h6> 
       </ion-row>
     </ion-item>
   </div>
  `
    this.addInfoWindow(marker, content); 
    marker.setMap(this.map); 
  
   } 


}).catch((error: any) => console.log(error));  
  

}

 

addMap(lat,long){ 

 let latLng = new google.maps.LatLng(lat, long);

 let mapOptions = {
 center: latLng,
 zoom: 15,
 mapTypeId: google.maps.MapTypeId.ROADMAP
 }

 this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 this.addMarker();

}

addMarker(){

let marker = new google.maps.Marker({
 map: this.map,
 animation: google.maps.Animation.DROP,
 position: this.map.getCenter()
});

let content = "<h4>This is your current position !!</h4>";         
this.addInfoWindow(marker, content); 

} 

addInfoWindow(marker, content){
 let infoWindow = new google.maps.InfoWindow({
   content:content
 })

 google.maps.event.addListener(marker, 'click', () => {
   infoWindow.open(this.map, marker);
 })

}

 

presentPrompt(lat:any,long:any) {

  let alert = this.alertCtrl.create({
    title: 'Meeting Remarks.',
    inputs: [
      {
        name: 'title',
        placeholder: 'Title' 
      } 
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },
      { 
        text: 'OK',
        handler: data => {
          if (data.title) { 

             this.timeINtitle=data.title; 
            //######## Insert Code Start Here #####################/

               let username = localStorage.getItem('username');
              let deviceid = localStorage.getItem('deviceid');  
              let TimeInCurrentID = localStorage.getItem('StartFromCurrentID');               
             let FirstClientLocation = localStorage.getItem('finalAddress');   
              this.loadPoints(lat,long);  

               console.log(`FirstClientLocation`,FirstClientLocation);
  
            let bodystring={
              ID:TimeInCurrentID,
              VisitDate :"",
              ODStatus :"", 
              UserLoginID :username, 
              CountManagerChange :"", 
              EmployeeEmailID :"", 
              FirstInTime :"",
              FirstOutTime :"",
              FirstLongitute :long,
              FirstLatitute :lat,
              DeviceName :deviceid,
              FirstClientLocation :FirstClientLocation,
              IDOutPut:1,
              Status:"",
              RemarkClientMeeting :this.timeINtitle,
              StartFromLocation:"",
              FirstClientLocationOut:"",
              LastClientLocation:""    
                }

            this.webservice.datafetch();

            this.webservice.SAVETimeINData(bodystring) 
            .then(response => {  

               console.log('data resp time in '+JSON.stringify(response));  
  
             this.webservice.loader.dismissAll();   

             let responsedata=JSON.stringify(response);
              let datadet=JSON.parse(responsedata);

      if(datadet.CurrentID!='' && datadet.CurrentID!=undefined 
          || datadet.LastInsertedDate!='' && datadet.LastInsertedDate!=undefined
          ||  datadet.ResultData!='' && datadet.ResultData!=undefined
          || datadet.StartInStatus!='' && datadet.StartInStatus!=undefined
          || datadet.TimeInStatus!='' && datadet.TimeInStatus!=undefined
          || datadet.TimeOutStatus!='' && datadet.TimeOutStatus!=undefined
          || datadet.FirstClientLocation!='' && datadet.FirstClientLocation!=undefined){
            
              this.CurrentID=datadet.CurrentID; 
              this.LastInsertedDate=datadet.LastInsertedDate; 
              this.ResultData=datadet.ResultData;             
              this.finalAddress=datadet.FirstClientLocation;
               localStorage.setItem('TimeInCurrentID', this.CurrentID);  
              localStorage.setItem('LastInsertedDate', this.LastInsertedDate); 
 
             
 if(datadet.StartInStatus==false && datadet.TimeInStatus==true && datadet.TimeOutStatus==true){
                           
                this.timeIStartFromResponse=false; 
              this.timeININsertResponse=true;
               this.timeOUTupdateResponse=true;
            
              }
             
            
             else if(datadet.StartInStatus==true && datadet.TimeInStatus==false && datadet.TimeOutStatus==true){
              
              this.timeIStartFromResponse=true; 
            this.timeININsertResponse=false;
             this.timeOUTupdateResponse=true;
            
            
            }  
            
            
            else if(datadet.StartInStatus==true && datadet.TimeInStatus==true && datadet.TimeOutStatus==false){
              

              this.timeIStartFromResponse=true; 
            this.timeININsertResponse=true;
             this.timeOUTupdateResponse=false;
            
            
            }  



               
              }
 
            
          }, (err) => { 
                        console.log("Error"+err);
                    }); 
          //####### End Here #############//
           } else { 
            // invalid login
            return false;
          }
        }
      }
    ]
  });
  alert.present();
} 

 

TimeOut(){

  this.webservice.Timeoutdatafetch();  


  this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 10000, enableHighAccuracy: true }).then((resp) => {
    
    this.LastlocationGPS = new LatLng(resp.coords.latitude, resp.coords.longitude); 

let LastLongitute = this.LastlocationGPS.lng; 
let LastLatitute = this.LastlocationGPS.lat;  
let deviceid = localStorage.getItem('deviceid');  
let TimeOutCurrentID = localStorage.getItem('TimeInCurrentID');   
let username = localStorage.getItem('username');  

this.loadPoints(LastLatitute,LastLongitute); 

let TimeOutFinaladdress = localStorage.getItem('TimeOutFinaladdress');  


   let timeoutbodystring={
        ID:TimeOutCurrentID,
        VisitDate :"", 
        ODStatus :true, 
        UserLoginID :username,
        CountManagerChange :1, 
        EmployeeEmailID :"", 
        LastInTime:"",
        LastOutTime :"",
        LastLongitute:LastLongitute, 
        LastLatitute:LastLatitute, 
        DeviceName :deviceid,
        LastClientLocation:TimeOutFinaladdress,  
        IDOutPut:1,
        Status:"",
        StartFromLocation:"",
        FirstClientLocation:"",
        LastClientLocationOut:""
  }
  
  
  this.webservice.UpdateTimePotData(timeoutbodystring) 
  .then(response => {   

    this.webservice.loader.dismissAll(); 
    if(response){  
        this.navCtrl.setRoot(OdPage); 
   }   
 
  
}, (err) => { 
              console.log("Error"+err);
          }); 


 
//   // send mail all information  
//   this.webservice.sendEmail(username,finalAddress,CurrentID,LastInsertedDate) 
//   .then(response => {   
//  console.log('response'+response);    
 
//   this.navCtrl.setRoot(HomePage, {  })  
 
// }, (err) => { 
//     console.log("Error"+err); 
// }); 
    

});  

}



APICall(){
  
  let username = localStorage.getItem('username');   

this.webservice.datafetch(); 

this.webservice.APICALLOD(username) 
.then(response => {    

  this.webservice.loader.dismissAll(); 

  let responsedata=JSON.stringify(response);

  let responseBody=JSON.parse(responsedata); 

  if(localStorage.getItem('StartFromCurrentID') =='' || localStorage.getItem('StartFromCurrentID')==null
     && localStorage.getItem('TimeInCurrentID') =='' || localStorage.getItem('TimeInCurrentID')==null
   ) { 
         localStorage.setItem('CurrentID', responseBody.CurrentID);   
         localStorage.setItem('StartFromCurrentID', responseBody.CurrentID);   
         localStorage.setItem('TimeInCurrentID', responseBody.CurrentID);   

   } 

  if(responseBody.StartInStatus==false && responseBody.TimeInStatus==true && responseBody.TimeOutStatus==true){
    
    this.timeIStartFromResponse=false; 
  this.timeININsertResponse=true;
   this.timeOUTupdateResponse=true;

  }
 

 else if(responseBody.StartInStatus==true && responseBody.TimeInStatus==false && responseBody.TimeOutStatus==true){
  
   this.timeIStartFromResponse=true; 
this.timeININsertResponse=false;
 this.timeOUTupdateResponse=true;

 
}  


else if(responseBody.StartInStatus==true && responseBody.TimeInStatus==true && responseBody.TimeOutStatus==false){
  
  this.timeIStartFromResponse=true; 
this.timeININsertResponse=true;
 this.timeOUTupdateResponse=false;

 
}  
 
 
}, (err) => { 
            console.log("Error"+err);
        }); 

 
} 


StartFrom(){
  
  
  this.webservice.datafetch();  

 
  this.geolocation.getCurrentPosition({ maximumAge: 3000, timeout: 10000, enableHighAccuracy: true }).then((resp) => {
    
    this.StartFromlocationGPS = new LatLng(resp.coords.latitude, resp.coords.longitude); 

 
    let StartFromLongitute = this.StartFromlocationGPS.lng; 
    let StartFromLatitute = this.StartFromlocationGPS.lat; 
    

    let deviceid = localStorage.getItem('deviceid');  
    let username = localStorage.getItem('username');  

    /***************************************************** */
 
  //  this.StartFromLocationfinal = localStorage.getItem('StartFromLocation');


    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
      }; 
    
    
    this.nativeGeocoder.reverseGeocode(StartFromLatitute,StartFromLongitute, options) 
    .then((result: NativeGeocoderReverseResult[]) =>      
    {

        this.countryCode=result[0].countryCode; 
        this.countryName=result[0].countryName;
        this.postalCode=result[0].postalCode;
        this.administrativeArea=result[0].administrativeArea;
        this.subAdministrativeArea=result[0].subAdministrativeArea;
        this.locality=result[0].locality;
        this.subLocality=result[0].subLocality; 

        this.StartFromLocation= 'State: '+this.administrativeArea+', ' +this.subAdministrativeArea+' , '+ this.locality 
                                + ' , '+this.subLocality+ ' ,  Country:  ' + this.countryName
                                +', Postal Code : '+this.postalCode;

        let StartFrombodystring={
          EmpCode:"",
          ClientName:"", 
          ClientLocation :"", 
          VisitDate :"", 
          RemarkClientMeeting :"", 
          ODStatus :"", 
          AssociateManager :this.ManagerName,  
          UserLoginID :username, 
          CountManagerChange :"", 
          EmployeeEmailID :"", 
          FirstInTime :"",
          FirstOutTime :"",
          LastInTime:"",
          LastOutTime :"",
          StartFromTime :"",
          FirstLongitute :"",
          FirstLatitute :"",
          LastLongitute:"", 
          LastLatitute:"", 
          StartFromLongitute :StartFromLongitute,
          StartFromLatitute :StartFromLatitute, 
          StartFromLocation : this.StartFromLocation,
          DeviceName :deviceid, 
          FirstClientLocation :"",        
          LastClientLocation:"",
          status:""        
        }
 
        this.webservice.SAVEStartFromData(StartFrombodystring) 
        .then(response => {  
         this.webservice.loader.dismissAll();   

         let responsedata=JSON.stringify(response);
          let datadet=JSON.parse(responsedata);

      if(datadet.CurrentID!='' && datadet.CurrentID!=undefined 
          || datadet.LastInsertedDate!='' && datadet.LastInsertedDate!=undefined
          ||  datadet.ResultData!='' && datadet.ResultData!=undefined
          || datadet.StartInStatus!='' && datadet.StartInStatus!=undefined
          || datadet.TimeInStatus!='' && datadet.TimeInStatus!=undefined
          || datadet.TimeOutStatus!='' && datadet.TimeOutStatus!=undefined
          || datadet.StartFromLocation!='' && datadet.StartFromLocation!=undefined){

          this.CurrentID=datadet.CurrentID;
          this.LastInsertedDate=datadet.LastInsertedDate;  
          this.ResultData=datadet.ResultData; 
          this.finalAddress=datadet.StartFromLocation; 
 
  

if(datadet.StartInStatus==false && datadet.TimeInStatus==true && datadet.TimeOutStatus==true){

 
 this.timeIStartFromResponse=false;  
this.timeININsertResponse=true;
this.timeOUTupdateResponse=true;

 }


else if(datadet.StartInStatus==true && datadet.TimeInStatus==false && datadet.TimeOutStatus==true){

 this.timeIStartFromResponse=true; 
this.timeININsertResponse=false;
this.timeOUTupdateResponse=true;

 
}  


else if(datadet.StartInStatus==true && datadet.TimeInStatus==true && datadet.TimeOutStatus==false){

 this.timeIStartFromResponse=true; 
this.timeININsertResponse=true;
this.timeOUTupdateResponse=false;

 
}   

          localStorage.setItem('StartFromCurrentID', this.CurrentID);               
          localStorage.setItem('LastInsertedDate', this.LastInsertedDate);                
          }  
      }, (err) => { 
                    console.log("Error"+err);
                });

 
    }).catch((error: any) => console.log(error));  
      
 
    /*******************************/ 
   
       
 
});  
    
}


  


}
