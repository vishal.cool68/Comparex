import { Component,ElementRef,Renderer, Input } from '@angular/core';
import { NavController, AlertController,Events, LoadingController, Loading,IonicPage, NavParams, Platform  } from 'ionic-angular';
 
import { WebserviceProvider } from '../../providers/webservice/webservice';
 import { HomePage } from '../home/home';
 import { FormBuilder, FormGroup, Validators } from '@angular/forms';

 
@Component({ 
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginPage {

 
  bodystring:any;
  username:any="";
  password:any="";
  web:any;
  submitAttempt=false;
   public showPass = false;
   public pass:String;
  public type = 'password';
form: FormGroup;
oauthToken:any;
depth: number = 0;

/** hide element **/
hideElementUsername:boolean=false;
hideElementPass:boolean=false;
loggedData:any;
deviceid:any;



  constructor(public navCtrl: NavController,
            private alertCtrl: AlertController,  public events: Events,
            private loadingCtrl: LoadingController,public navParams: NavParams,public platform: Platform,
            public formBuilder: FormBuilder,private elRef: ElementRef,private renderer: Renderer,
             private webservice: WebserviceProvider) { 

     
              if (!this.platform.is('android')) { 
                localStorage.setItem("deviceType","android");  
              } 
              
              // this.form = formBuilder.group({   
              //   username: ['', Validators.compose([Validators.required])],
              //   password: ['', Validators.compose([Validators.required])]  
              // });     
                 
              
              this.form = formBuilder.group({  
                username: ['', Validators.compose([Validators.required])],
                password: ['', Validators.compose([Validators.required])]  
              });    
                 
    } 
 
    ionViewDidLoad() {
      console.log('ionViewDidLoad LoginpagePage'); 
  
     } 


     onlogin(){ 
      this.callLogin();
     }


callLogin(){ 

  if(!this.form.valid ){
    this.submitAttempt = true;
    return;
  }
  
    let username= this.form.controls.username.value;
    let password= this.form.controls.password.value; 
     
    this.webservice.login_user_get(username,password) 
     .then(response => { 

      let data=JSON.stringify(response); 
      let loginData=JSON.parse(data); 

     // loginData.access_token='successfull';

      console.log('data'+data);
      if(loginData.access_token=='successfull'){
        
        this.loggedData=data;  

        localStorage.setItem('loggedData', this.loggedData);  
        localStorage.setItem('username', username);  
        this.events.publish("Update:loggedStatus");  

        this.navCtrl.setRoot(HomePage, {  }) 


      }else{

        this.webservice.showAlert('Invalid Login Credentials.');
      }
       
          
    }, (err) => { 
                    console.log("Error"+err);
                }); 
          

}

forgotpwd(){

// this.navCtrl.push(ForgotPasswordPage, {
//   }) 
 
}


forgotusername(){
 // this.navCtrl.push(ForgotRegisteredNumberPage, {
  //  }) 

}
 
  // public login() {
  //   this.showLoading()
    
  //   this.webservice.login(this.loginCredentials).subscribe(allowed => {
		
	// 	 // console.log('allowed'+allowed); 
		  
  //     if (allowed) {  
		  
  //       setTimeout(() => {
  //       this.loading.dismiss();
  //       this.nav.setRoot(HomePage) 
  //       localStorage.setItem("loggedInStatus", "active");
  //       });
  //     } else {
  //       localStorage.setItem("loggedInStatus", "inactive");
  //       this.showError("Access Denied"); 
  //     }
  //   },
  //   error => {
  //     this.showError(error);
  //   });
  // }
 
  // showLoading() {
  //   this.loading = this.loadingCtrl.create({
  //     content: 'Please wait...' 
  //   });
  //   this.loading.present();
  // }
 
  // showError(text) {
  //   setTimeout(() => {
  //     this.loading.dismiss();
  //   });
 
  //   let alert = this.alertCtrl.create({
  //     title: 'Fail',
  //     subTitle: text,
  //     buttons: ['OK']
  //   });
  //   alert.present(prompt);
  // }

 


}