import { Component,ViewChild, ElementRef  } from '@angular/core'; 
  import { Content } from 'ionic-angular';

import {Nav, NavController, ActionSheetController, LoadingController,Platform } from 'ionic-angular';
  
import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions,LatLng, CameraPosition, MarkerOptions, Marker,MyLocation,GoogleMapsAnimation} from '@ionic-native/google-maps'; 

import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation'; 
import { JsonPipe } from '@angular/common';
 
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderForwardResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
     
import { WebserviceProvider } from '../../providers/webservice/webservice';

import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network'; 
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmailComposer } from '@ionic-native/email-composer';
import { Http} from '@angular/http'; 
import { Headers, RequestOptions } from '@angular/http';
import { AlertController } from 'ionic-angular';
import { OdPage } from '../od/od';

declare var google; 

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  @ViewChild(Content) content: Content;

  scrollToTop() {
    this.content.scrollToTop(); 
  }

  
  srcImage: string;
  OCRAD: any;
  @ViewChild('map') mapElement: ElementRef; 
  map: any;
  // map: GoogleMap; 
  options : GeolocationOptions;
  currentPos : Geoposition; 
  value:any;
  lat:any;
  long:any;

  markers: any;  
 finaladdress:any; 
 
establishments:any;

countryCode:any;
countryName:any;
postalCode:any;
administrativeArea:any;
subAdministrativeArea:any;
locality:any;
subLocality:any; 
finalAddress:any;
email:string; 
profileModel: string = "profile";
isAndroid: boolean = false; 
timeINtitle:any;

timeININsertResponse=false;
timeOUTupdateResponse=false;
saveFinaladdress: Array<{}>; 

CurrentID:any; 
LastInsertedDate:any; 
ResultData:any;

LastlocationGPS:any;
StartFromlocationGPS:any;
lastlat:any;
lastlong:any;
UserName:any;
UserLoginID:any;
ManagerName:any;
userresult:any;
//showUserDetails:boolean; 
showUserDetails=false; 
selectedSection:any;

 
  constructor(public navCtrl: NavController,
    public platform: Platform,private device: Device,private network: Network,
    private googleMaps: GoogleMaps,
    private locationAccuracy: LocationAccuracy,
    private iab: InAppBrowser,private geolocation: Geolocation 
    ,private nativeGeocoder: NativeGeocoder,
    private webservice: WebserviceProvider,
    private emailComposer: EmailComposer,
    public http: Http,
    private alertCtrl: AlertController ) {   

      this.isAndroid = platform.is('android');

      let loggedData = JSON.parse(localStorage.getItem('loggedData'));   

      this.email=loggedData.EmailID; 
      this.UserName=loggedData.UserName;
      this.UserLoginID=loggedData.UserLoginID;
      this.ManagerName=loggedData.ManagerName;


     
   }
 
  ionViewDidLoad(){

    this.UserAllListDetails();


  }
 
  UserAllListDetails(){

    let username = localStorage.getItem('username');   

    this.webservice.datafetch();
     this.webservice.ALLUsersList(username) 
    .then(response => {  
      this.webservice.loader.dismissAll(); 
      console.log('user response',JSON.stringify(response));
      this.userresult=response;  

  }, (err) => { 
      console.log("Error"+err);
  }); 


  }

 

}
