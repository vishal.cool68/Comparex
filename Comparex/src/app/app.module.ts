import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { FormsModule } from '@angular/forms';
 import { HttpModule } from '@angular/http';
  
 
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network'; 
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { GoogleMaps, GoogleMap, GoogleMapsEvent, GoogleMapOptions,LatLng, CameraPosition, MarkerOptions, Marker,MyLocation,GoogleMapsAnimation} from '@ionic-native/google-maps'; 

import { EmailComposer } from '@ionic-native/email-composer';

import { Geolocation ,GeolocationOptions ,Geoposition ,PositionError } from '@ionic-native/geolocation'; 
 
import { NativeGeocoder,NativeGeocoderReverseResult,NativeGeocoderForwardResult } from '@ionic-native/native-geocoder'; 

   import { AndroidPermissions } from '@ionic-native/android-permissions';
import { WebserviceProvider } from '../providers/webservice/webservice';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
 import { LoginPage } from '../pages/login/login';  
 import { ManagerodapprovalPage } from '../pages/managerodapproval/managerodapproval';
 import { OdPage } from '../pages/od/od';  


 
@NgModule({
  declarations: [
    MyApp,
    HomePage,
     LoginPage,
     ManagerodapprovalPage,
     OdPage  
  ], 
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    IonicModule.forRoot(MyApp), 
   ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
     LoginPage,
     ManagerodapprovalPage,
     OdPage
  ],
  providers: [
    StatusBar,
    SplashScreen,Network,Device,LocationAccuracy,InAppBrowser,Geolocation,EmailComposer ,
    AndroidPermissions,NativeGeocoder,GoogleMaps,
    {provide: ErrorHandler, useClass: IonicErrorHandler}, 
    WebserviceProvider 
  ]
})
export class AppModule {}
