import { Component, ViewChild } from '@angular/core';
import { Nav,Menu, NavController,Platform,MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Device } from '@ionic-native/device';
import { Network } from '@ionic-native/network'; 
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Geolocation } from '@ionic-native/geolocation';
 

import { HomePage } from '../pages/home/home'; 
 import { LoginPage } from '../pages/login/login'; 
 import { ManagerodapprovalPage } from '../pages/managerodapproval/managerodapproval';
 import { OdPage } from '../pages/od/od';

 

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
 
 
  @ViewChild('content') content: NavController;
  @ViewChild(Nav) nav: Nav; // added by vishal for Menu
  @ViewChild(Menu) menu: Menu; 
  pages: Array<{title: string, component: any,icon: string}>; 

  rootPage: any = LoginPage;  
  deviceid:any;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
    private device: Device,private network: Network,private locationAccuracy: LocationAccuracy,
    private iab: InAppBrowser,private geolocation: Geolocation,public menuCtrl: MenuController ) {
    
    this.initializeApp();   
  

// watch network for a disconnect
let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
  console.log('network was disconnected :-(');
});

// stop disconnect watch
disconnectSubscription.unsubscribe();


// watch network for a connection
let connectSubscription = this.network.onConnect().subscribe(() => {
  console.log('network connected!');
  // We just got a connection but we need to wait briefly
   // before we determine the connection type. Might need to wait.
  // prior to doing any api requests as well.
  setTimeout(() => {
    if (this.network.type === 'wifi') {
      console.log('we got a wifi connection, woohoo!');
    }
  }, 3000); 
});
 
// stop connect watch
connectSubscription.unsubscribe();


this.locationAccuracy.canRequest().then((canRequest: boolean) => {

  if(canRequest) {
    // the accuracy option will be ignored by iOS
    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
      () => console.log('Request successful'),
      error => console.log('Error requesting location permissions', error)
    );
  }   
      
}); 
 
    // used for an example of ngFor and navigation 
    // this.pages = [
    //  // attendance
    //   { title: 'Dashboard', component: HomePage, icon:'home' },  
    //   { title: 'OD Request', component: OdPage , icon:'assets/imgs/OD.png'},
    //   { title: 'Manager OD ApprovalPage', component: ManagerodapprovalPage , icon:''},
    //   { title: 'Attendance', component: '' , icon:'assets/imgs/attendance.png' },
    //   { title: 'Comp Off', component: '' , icon:''},
    //   { title: 'Leave Request', component: 'HomePage' , icon:'assets/imgs/leave.png'},
    //  ]; 

  }

  initializeApp() {
    this.platform.ready().then(() => {  
      console.log('Device UUID is: ' + this.device.uuid); 
      // this.deviceid=this.device.uuid;
      // var model = this.device.model;
      // var deviceID = this.device.uuid; 
      // var string = this.device.version; 

      this.deviceid= 'Device ID : '+ this.device.uuid + ', Device Model : ' +this.device.model + ', Device Version : ' +this.device.version;
      localStorage.setItem("deviceid",this.deviceid);  
 
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
 
    });  
  }

  

  logOut(){
    localStorage.clear();
    this.content.setRoot(LoginPage); 
    // this.rootPage = LoginPage;  
  }
  
  // openPage(page) { 
  //       this.nav.setRoot(page.component);  
  //     // this.content.setRoot(page.component); 
  //      for (let p of this.pages) { 
  //      if(p.title==page.title)
  //      { }
  //      else
  //      {  }

  //      }

  //    }


 

     openDashboard(){
      this.nav.setRoot(HomePage);  
     }
     openOD(){
      this.nav.setRoot(OdPage);  
     }
     openManagerODApp(){
      this.nav.setRoot(ManagerodapprovalPage);  
     } 
     openAttendance(){
      this.nav.setRoot(HomePage);  
     }
     openCompOff(){
      this.nav.setRoot(HomePage);  
     }

     openLeave(){
      this.nav.setRoot(HomePage);  
     }



}
